import Angular from 'angular';
import AngularBootstrap from 'angular-ui-bootstrap';
import ngAnimate from 'angular-animate';
import ngSanitize from 'angular-sanitize';
import ngTouch from 'angular-touch';

import NavView from '../templates/nav-view.html';

import UserService from '../services/user.js'
import SessionStorage from 'angular-sessionstorage'


let app = Angular.module('nav.navbarRightCtrl', [SessionStorage, AngularBootstrap, ngAnimate, ngSanitize, ngTouch, UserService]);

app.controller("navbarRightCtrl",
	["$rootScope","$scope","$http","$location","$sessionStorage", 'user',function($rootScope, $scope,$http,$location, $sessionStorage, UserService){
	var ctrl = this;
	ctrl.credentials = {};
	// ctrl.credentials = {username: "lukasz.bartocha@gmail.com", password: "lukasz123"}
	//ctrl.credentials = {username: "adam.adamowski@gmail.com", password: "adam123"}

	$scope.unloggedUser = UserService.isUnloggedUser();
	if (!$scope.unloggedUser) {
		ctrl.credentials.username=UserService.getUsername()
	}
	ctrl.loginUser = function(keyEvent=undefined){
		if (keyEvent===undefined || keyEvent.which === 13) {
			UserService.log(ctrl.credentials.username, ctrl.credentials.password);
		}
  };

	let registerScope1 = $rootScope.$on('user.log', function(event, data) {
		if (data.type === 'success') {
			ctrl.isUnLoggedUser();
		 $location.path('/home');
	 	}
		ctrl.credentials.password="";
	});
	$scope.$on('$destroy', registerScope1);

	// ctrl.goToRegisterPage = function(){
	// 	$location.path("/register");
	// };
	ctrl.goToAcountDetailsPage = function(){
		$location.path("/account");
	};

	ctrl.goToUserGroupsPage = function(){
		$location.path("/user/groups");
	};

	ctrl.goToUserPermGroupProfilePage = function(){
		$location.path("/user/accessprofiles");
	};

	ctrl.goToDocumentPage = function(){
		$location.path("/document");
	};


	ctrl.logoutUser = function(){
		UserService.logout();
		ctrl.credentials.username="";
		$location.path('/home');
		// $scope.unloggedUser = true;
		ctrl.isUnLoggedUser();
		};
	ctrl.isUnLoggedUser = function(){
		return UserService.isUnloggedUser();
	}

  }]);

export default app.name;
