import Angular from 'angular';

import ExerciseService from '../services/exercise.js';
import DocumentService from '../services/document.js';

import SessionStorage from 'angular-sessionstorage';
import CategoryService from '../services/categories.js';
import UserService from '../services/user.js';
import { Base64 } from 'js-base64';

import FileDirective from '../directives/file-directive.js';
import DocumentUpdateComponent from '../components/update-document-component.js';
import DocumentrecipeCreateComponent from '../components/create-document-recipe-component.js';
import DocumentCreateComponent from '../components/create-document-component.js';


let app = Angular.module('controllers.document', [DocumentService, ExerciseService, CategoryService, FileDirective, DocumentUpdateComponent, DocumentrecipeCreateComponent, DocumentCreateComponent])
  .controller('DocumentCtrl',['$uibModal', "$scope", "$element", '$rootScope','exercise', 'category', 'user','$log', '$compile', '$window', 'document', '$location', '$routeParams',
  function($uibModal, $scope, $element, $rootScope, ExerciseService, CategoryService, UserService, $log, $compile, $window, DocumentService, $location, $routeParams){
  var $ctrl = this;

  $ctrl.getDocuments = function(){

    let search_query = "";
    if ($ctrl.advanced_search == true) {
      Angular.forEach($ctrl.adv_search, function(value, key) {
        search_query = search_query +'&'+ key + '='+value;
      });
    } else if ($ctrl.advanced_search == false) {
      let simply = $ctrl.sim_search.simply;
      let operation = $ctrl.sim_search.operation;
      search_query = '&documenttitle='+simply+
                     '&author='+simply+
                     '&filename='+simply+
                     '&name='+simply+
                     '&description='+simply+
                     '&operation='+operation;
    }
    let query = '?page='+$ctrl.bigCurrentPage+'&pagesize='+$ctrl.apiPageSize+search_query;

    DocumentService.getDocuments('document.getDocuments', query);
  };

  let registerScope5 = $rootScope.$on('document.getDocuments', function(event, data) {
    if (data.type === 'success') {
      $scope.documents=data.msg;
      $scope.documents=data.msg.documents;
      $ctrl.bigTotalItems=data.msg.total_documents_num;
    }
  });
  $scope.$on('$destroy', registerScope5);

  $ctrl.deleteDocument = function (document_id) {
    DocumentService.deleteDocument('document.deleteDocument',
                                          document_id);
  };

  let registerScope6 = $rootScope.$on('document.deleteDocument', function(event, data) {
    if (data.type === 'success') {
      if ($ctrl.multipleDocumentMode) {
        $ctrl.getDocuments();
      }
      else {
          $location.path("/document");
      }
    }

  });
  $scope.$on('$destroy', registerScope6);

  $ctrl.generatePdfDocumentAndOpen = function (document_id) {
    DocumentService.generatePdfDocument('document.generatePdfDocument.open',
                                          document_id);

  };

  $ctrl.generatePdfDocumentAndDownload = function (document_id) {
    DocumentService.generatePdfDocument('document.generatePdfDocument.download',
                                          document_id);
  };

  $ctrl.generateZipPackageAndDownload = function (document_id) {
    DocumentService.generateZipPackage('document.generateZipPackage.download',
                                          document_id);
  };


  let registerScope1 = $rootScope.$on('document.generatePdfDocument.open', function(event, data) {
    if (data.type === 'success') {
      let newWindow = $window.open("",);
      newWindow.document.write('<iframe src="data:application/pdf;base64,' + (data.msg.base64_string) + '" width="100%" height="100%"></iframe>');
      newWindow.document.title = data.msg.filename;
      console.log($rootScope.currentStatus);
      console.log(event);
      console.log('----------');
    }
  });

  let registerScope2 = $rootScope.$on('document.generatePdfDocument.download', function(event, data) {
    if (data.type === 'success') {
      var downloadLink = document.createElement("a");
      downloadLink.href = "data:application/pdf;base64," + (data.msg.base64_string);
      downloadLink.download = data.msg.filename;
      document.body.appendChild(downloadLink);
      downloadLink.click();
      document.body.removeChild(downloadLink);
    }
  });

  let registerScope3 = $rootScope.$on('document.generatePdfDocument.open.error', function(event, data) {
    console.log("Problem przy generowaniu dokumentu");
    console.log(data.msg);
    console.log(data.error);
  });

  let registerScope4 = $rootScope.$on('document.generatePdfDocument.download.error', function(event, data) {
    console.log("Problem przy generowaniu dokumentu");
    console.log(data.msg);
    console.log(data.error);
  });

  $scope.$on('$destroy', registerScope1);
  $scope.$on('$destroy', registerScope2);
  $scope.$on('$destroy', registerScope3);
  $scope.$on('$destroy', registerScope4);

  let registerScope2_tex = $rootScope.$on('document.generateZipPackage.download', function(event, data) {
    if (data.type === 'success') {
      var downloadLink = document.createElement("a");
      downloadLink.href = "data:application/zip;base64," + (data.msg.base64_string);
      downloadLink.download = data.msg.filename;
      document.body.appendChild(downloadLink);
      downloadLink.click();
      document.body.removeChild(downloadLink);
    }
  });

  let registerScope4_tex = $rootScope.$on('document.generateZipPackage.download.error', function(event, data) {
    console.log("Problem przy generowaniu zip");
    console.log(data.msg);
    console.log(data.error);
  });

  $scope.$on('$destroy', registerScope2_tex);
  $scope.$on('$destroy', registerScope4_tex);


  $ctrl.modalDocumentRecipeComponentCreate = function (selectedData) {
    let objCopy = angular.copy(selectedData);
    var modalInstance = $uibModal.open({
      animation: true,
      component: 'modalDocumentRecipeComponentCreate',
      resolve: {
        items: function () {
          return objCopy;
        }
      }
    });
    modalInstance.result.then(function (selectedItem) {
      // $ctrl.selected = selectedItem;
    }, function () {
      $log.info('modal-component dismissed at: ' + new Date());
    });
  };

  let registerScope7 = $rootScope.$on('Document.updateDocumentRecipe.modal', function(event, data) {
    if (data.type === 'success') {
      $ctrl.getDocuments();
    }
  });
  $scope.$on('$destroy', registerScope7);

  $ctrl.deleteDocumentRecipe = function (document_id, exercise_recipe_id) {
    DocumentService.deleteDocumentRecipe('document.deleteExerciseRecipe',
                                          document_id, exercise_recipe_id);
  };

  let registerScope8 = $rootScope.$on('document.deleteExerciseRecipe', function(event, data) {
    if (data.type === 'success') {
      if ($ctrl.multipleDocumentMode) {
        $ctrl.getDocuments();
      }
      else {
        $ctrl.getSingleDocument($routeParams['documentId']);
      }
    }
  });
  $scope.$on('$destroy', registerScope8);

  $ctrl.modalDocumentComponentCreate = function () {
    let objCopy = angular.copy({});
    var modalInstance = $uibModal.open({
      animation: true,
      component: 'modalDocumentComponentCreate',
      resolve: {
        items: function () {
          return objCopy;
        }
      }
    });
    modalInstance.result.then(function (selectedItem) {
      // $ctrl.selected = selectedItem;
    }, function () {
      $log.info('modal-component dismissed at: ' + new Date());
    });
  };

  let registerScope9 = $rootScope.$on('Document.createDocument.modal', function(event, data) {
    if (data.type === 'success') {
      $ctrl.getDocuments();
    }
  });
  $scope.$on('$destroy', registerScope9);

  $ctrl.modalDocumentComponentUpdate = function (selectedData) {
    let objCopy = angular.copy(selectedData);
    var modalInstance = $uibModal.open({
      animation: true,
      component: 'modalDocumentComponentUpdate',
      resolve: {
        items: function () {
          return objCopy;
        }
      }
    });
    modalInstance.result.then(function (selectedItem) {
      // $ctrl.selected = selectedItem;
    }, function () {
      $log.info('modal-component dismissed at: ' + new Date());
    });
  };

  let registerScope10 = $rootScope.$on('Document.updateDocument.modal', function(event, data) {
    if (data.type === 'success') {
      if ($ctrl.multipleDocumentMode) {
        $ctrl.getDocuments();
      }
      else {
        $ctrl.getSingleDocument($routeParams['documentId']);
      }
    }
  });
  $scope.$on('$destroy', registerScope10);

  $ctrl.goToSingleExercisePage = function(exercise_id){
    window.open("exercise/"+exercise_id, '_blank');
    // $location.path("/exercise/"+$ctrl.items.exercise_recipes[index].exercise.id);
  }
  $ctrl.goToSingleDocumentPage = function(document_id){
    window.open("document/"+document_id, '_blank');
    // $location.path("/exercise/"+$ctrl.items.exercise_recipes[index].exercise.id);
  }

  $ctrl.getSingleDocument = function(document_id) {
    DocumentService.getDocument("Document.getSingleDocument",
                                document_id);
  }

  let registerScope11 = $rootScope.$on('Document.getSingleDocument', function(event, data) {
    if (data.type === 'success') {
      $scope.documents=[data.msg];
      $ctrl.bigTotalItems=1;
    }
  });
  $scope.$on('$destroy', registerScope11);
  let registerScope12 = $rootScope.$on('Document.getSingleDocument.notfound', function(event, data) {
    $location.path("/pageNotFound");
  });
  $scope.$on('$destroy', registerScope12);

  $ctrl.clear_search = function(){
    $ctrl.adv_search = {
      "documenttitle": "",
      author: "",
      filename: "",
      name: "",
      description: "",
      operation: "and"
    };
    $ctrl.sim_search = {
      simply: "",
      operation: "or"
    }

  };
  $ctrl.clear_search();
  $ctrl.advanced_search = false;
  if ($routeParams.hasOwnProperty('documentId')) {
    $ctrl.singleDocumentMode=true;
    $ctrl.multipleDocumentMode=false;
  }
  else {
    $ctrl.singleDocumentMode=false;
    $ctrl.multipleDocumentMode=true;
  }

  $scope.maxSize = 3;
  $ctrl.bigTotalItems = 175;
  $ctrl.bigCurrentPage = 1;
  $ctrl.apiPageSize = 2;

  $ctrl.options = {
    apiPageSize: [2, 4, 5, 10, 20, 40, 60, 90]};

  $ctrl.myInit = function(){
    if ($ctrl.multipleDocumentMode) {
      $ctrl.getDocuments();

    }
    else {
      $ctrl.getSingleDocument($routeParams['documentId']);
    }
  }
}]);

export default app.name;
