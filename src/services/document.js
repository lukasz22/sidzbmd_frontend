import Angular from 'angular';

let app = Angular.module('service.document', []);

app.service('document',
  ["$domainName","$http","$location","$sessionStorage","$rootScope", '$log',
  function($domainName,$http, $location, $sessionStorage, $rootScope, $log) {
    let DOMAIN_NAME=$domainName;
    let documents = this;
    this.getAllDocuments = function (message) {
      $http.get(DOMAIN_NAME+'document/',{
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      });
    };
    this.getDocuments = function (message, query) {
      $log.debug(message);
      $http.get(DOMAIN_NAME+'document/find/'+query,{
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      });
    };

    this.getDocument = function (message, document_id) {
      $log.debug(message);
      $http.get(DOMAIN_NAME+'document/'+document_id+'/',{
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      }, function() {
        $rootScope.$emit(message+'.notfound', {type: 'danger', msg: "Dokument nie istnieje"});
    });
    };

    this.createDocument = function (message, document) {
      console.log(document);
      console.log("aaaaaaaaaaa");
      for (var index of Object.keys(document.exercise_recipes)) {
        document.exercise_recipes[index].exercise = document.exercise_recipes[index].exercise.id;
      }
      console.log(document);
      $http.post(DOMAIN_NAME+'document/', document, {
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      }, function(response){
        documents.validate_400_response(message, response)
      });
    };

    this.updateDocument = function (message, document) {

      for (var index of Object.keys(document.exercise_recipes)) {
        document.exercise_recipes[index].exercise = document.exercise_recipes[index].exercise.id;
      }
      console.log(document);
      $http.put(DOMAIN_NAME+'document/'+document.id+'/', document, {
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      }, function(response){
        documents.validate_400_response(message, response)
      });
    };

    this.deleteDocument = function (message, document_id) {
      $http.delete(DOMAIN_NAME+'document/'+document_id+'/', {
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      });
    };

    this.deleteDocumentRecipe = function (message, document_id, recipe_id) {
      $http.delete(DOMAIN_NAME+'document/'+document_id+'/recipe/'+recipe_id+'/', {
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      });
    };

    this.createDocumentRecipe = function (message, recipe_data) {
      console.log(recipe_data);
      recipe_data.exercise = recipe_data.exercise.id;
      recipe_data.document = recipe_data.selectedType.id;
      $http.post(DOMAIN_NAME+'document/'+recipe_data.document+'/recipe/', recipe_data, {
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      }, function(response){
        documents.validate_400_response(message, response)
      });
    };

    this.generatePdfDocument = function (message, document_id) {
      $http.post(DOMAIN_NAME+'document/'+document_id+'/generate/',{}, {
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      }, function(response) {
        if (response.status === 400) {
          $rootScope.$emit(message+'.error', {type: 'invalid', msg: response.data,
                        error: response.data});
        }
        else {
          $rootScope.$emit(message+'.error', {type: 'danger', msg: "Problem przy generowaniu",
                        error: response.data});
          console.log('inny kod bledu');
        }

    });
    };

    this.generateZipPackage = function (message, document_id) {
      $http.post(DOMAIN_NAME+'document/'+document_id+'/generatetex/',{}, {
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      }, function(response) {
        if (response.status === 400) {
          $rootScope.$emit(message+'.error', {type: 'invalid', msg: response.data,
                        error: response.data});
        }
        else {
          $rootScope.$emit(message+'.error', {type: 'danger', msg: "Problem przy generowaniu",
                        error: response.data});
          console.log('inny kod bledu');
        }
    });
    };

    this.validate_400_response = function(message, response){
      if (response.status === 400) {
        $rootScope.$emit(message, {type: 'novalid', msg: response.data});
      }
      else {
        console.log('inny kod bledu');
      }
    }

}]);


export default app.name;
