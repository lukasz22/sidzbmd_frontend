import Angular from 'angular';

let app = Angular.module('shared.services', []);

app.service('user',
  ['$domainName', "$http","$location","$sessionStorage","$rootScope", "$window",
  function($domainName, $http, $location, $sessionStorage, $rootScope, $window) {
    console.log("siemka");
    let DOMAIN_NAME=$domainName;
    let users = this;
    // let DOMAIN_NAME='http://strengthofhabit.eu-gb.mybluemix.net/';
    this.log = function (username, password) {
      let credentials = {};
      credentials.username = username;
      credentials.password = password;

      $http.post(DOMAIN_NAME+'api-token-auth/',
        credentials).then(function(response){
          console.log(response);
          let token = response.data['token'];
          let is_admin = response.data['is_administrator'];
          // credentials = {};
          $sessionStorage.put('user_token', token);
          $sessionStorage.put('username_sidzbmd', username);
          $sessionStorage.put('is_administrator_sidzbmd', is_admin);
          $rootScope.$emit('user.log', {type: 'success', msg: 'Uzytkownik został zalogowany'});
      }, function() {
        $rootScope.$emit('user.log', {type: 'danger', msg: "Błędny login lub hasło"});
        $sessionStorage.remove('username_sidzbmd');
        // console.log("Stub backend :)");
        // $sessionStorage.put('user_token', 'temporary_token');
        // $sessionStorage.put('username_sidzbmd', username);
    });
    };

    this.logout = function () {
      $sessionStorage.remove('user_token');
      $sessionStorage.remove('username_sidzbmd');
      $sessionStorage.remove('is_administrator_sidzbmd');
      $window.localStorage.clear();
      $rootScope.$emit('user.logout', {type: 'success', msg: 'Uzytkownik zostal wylogowany'});
    }

    this.update = function (nick, birth_year, surname, name) {
      let accountDetails = {};
      accountDetails.nick=nick;
      accountDetails.birth_year=birth_year;
      accountDetails.surname=surname;
      accountDetails.name=name;

      $http.put(DOMAIN_NAME+'ZTI_Project/app/user', accountDetails,{headers: {'X-WWW-Authenticate': $sessionStorage.get('user_token'), "Access-Control-Allow-Methods": "GET, POST, OPTIONS, PUT, DELETE"}
      }).then(function(response){
          $rootScope.$emit('user.update', {type: 'success', msg: response.data.message});
      });
    }

    this.delete = function () {
      $http.delete(DOMAIN_NAME+'ZTI_Project/app/user',{headers: {'X-WWW-Authenticate': $sessionStorage.get('user_token'), "Access-Control-Allow-Methods": "GET, POST, OPTIONS, PUT, DELETE"}
      }).then(function(response){
          $sessionStorage.remove('user_token');
          $rootScope.$emit('user.delete', {type: 'success', msg: response.data.message});
      });
    }


    this.getAll = function(){
      $http.get(DOMAIN_NAME+'ZTI_Project/app/user/users/all',{
        headers: {'X-WWW-Authenticate': $sessionStorage.get('user_token')}
      }).then(function(response){
          $rootScope.$emit('user.getAll', {type: 'success', msg: response.data});
      });

    }

    this.isUnloggedUser = function() {
      let user_token = $sessionStorage.get('user_token');
      if (user_token) {
        return false;
      }
      else {
        return true;
      }
    }

    this.getUsername = function() {
      let username = $sessionStorage.get('username_sidzbmd');
      return username;
    }

    this.isAdministrator = function() {
      let is_administrator = $sessionStorage.get('is_administrator_sidzbmd');
      if (is_administrator === 'true') {
        is_administrator=true;
      }
      else {
        is_administrator=false;
      }
      return is_administrator;
    }

    this.getLoggedUserData = function(message) {
      $http.get(DOMAIN_NAME+'ZTI_Project/app/user/',{
        headers: {'X-WWW-Authenticate': $sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      });
    }

    this.getGroups = function(message) {
      $http.get(DOMAIN_NAME+'user/groups/',{
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      });
    }
    this.getAllGroups = function(message) {
      $http.get(DOMAIN_NAME+'groups/',{
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      });
    }

    this.getAccessGroupsProfile = function(message) {
      $http.get(DOMAIN_NAME+'user/exercise/permission/',{
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      });
    }

    this.getAccessGroupsProfileByQuery = function(message, query) {
      $http.get(DOMAIN_NAME+'user/exercise/permission/find/'+query,{
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      });
    }

    this.getSingleAccessGroupsProfile = function(message, user_permission_id) {
      $http.get(DOMAIN_NAME+'user/exercise/permission/'+user_permission_id+'/',{
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      }, function() {
        $rootScope.$emit(message+'.notfound', {type: 'danger', msg: "Zadanie nie istnieje"});
    });
    }

    this.updateAccessGroupsProfile = function(message, profile, selected_group) {
      // console.log(profile);
      profile.group = selected_group.id;
      $http.put(DOMAIN_NAME+'user/exercise/permission/'+profile.id+'/', profile, {
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      }, function(response){
        users.validate_400_response(message, response)
      });
    }

    this.deleteAccessGroupsProfile = function(message, profile) {
      $http.delete(DOMAIN_NAME+'user/exercise/permission/'+profile.id+'/', {
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      });
    }

    this.createAccessGroupsProfile = function(message, profile, selected_group) {
      // console.log(profile);
      profile.group = selected_group.id;
      $http.post(DOMAIN_NAME+'user/exercise/permission/', profile, {
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      }, function(response){
        users.validate_400_response(message, response)
      });
    }

    this.validate_400_response = function(message, response){
      if (response.status === 400) {
          // if (response.data.hasOwnProperty("parent") &&
          //   response.data.parent[0] === "parent error") {
          //     response.data.parent = "Podałeś niewłaściwego rodzica"
          // }
          // if (response.data.hasOwnProperty("category_name") &&
          //   response.data.category_name[0] === "category with this category name already exists.") {
          //     response.data.category_name = "Podana kategoria już istnieje"
          // }
        $rootScope.$emit(message, {type: 'novalid', msg: response.data});
      }
      else {
        console.log('inny kod bledu');
      }
    }

}]);


export default app.name;
