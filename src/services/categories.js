import Angular from 'angular';

let app = Angular.module('shared.categories', []);

app.service('category',
  ["$domainName","$http","$location","$sessionStorage","$rootScope",
  function($domainName, $http, $location, $sessionStorage, $rootScope) {
    let DOMAIN_NAME=$domainName;
    let categories = this;

    this.getAllCategoriesTree = function (message) {
      $http.get(DOMAIN_NAME+'category/treeall/',{

        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      });
    };

    this.getAllCategories = function (message) {
      $http.get(DOMAIN_NAME+'category/',{

        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      });
    };

    this.getAllLanguages = function (message) {
      $http.get(DOMAIN_NAME+'languages/',{

        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      });
    };


    this.deleteSingleCategory = function (message, category_id) {
      $http.delete(DOMAIN_NAME+'category/'+category_id+'/',{

        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      },
      function(response){
        categories.validate_400_response(message, response)
      });
    };

    this.addCategory = function (message, category) {
      category.children = null;
      category.full_name = null;
      if (category.parent.id === 'all') {
        category.parent = null;
      }
      else {
          category.parent = category.parent.id
      }
      $http.post(DOMAIN_NAME+'category/', category, {

        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      },
      function(response){
        categories.validate_400_response(message, response)
      });
    };


    this.updateCategory = function (message, category) {
      console.log(category);
      category.children = null;
      category.full_name = null;
      if (category.parent.id === 'all') {
        category.parent = null;
      }
      else {
          category.parent = category.parent.id
      }
      $http.put(DOMAIN_NAME+'category/'+category.id+'/', category, {

        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      },
      function(response){
        categories.validate_400_response(message, response)
      });
    };

    this.validate_400_response = function(message, response){
      if (response.status === 400) {
          if (response.data.hasOwnProperty("parent") &&
            response.data.parent[0] === "parent error") {
              response.data.parent = "Podałeś niewłaściwego rodzica"
          }
          if (response.data.hasOwnProperty("category_name") &&
            response.data.category_name[0] === "category with this category name already exists.") {
              response.data.category_name = "Podana kategoria już istnieje"
          }
        $rootScope.$emit(message, {type: 'novalid', msg: response.data});
      }
      else if (response.status === 403) {
        $rootScope.$emit(message, {type: 'danger', msg: response.data});
      }
      else {
        console.log('inny kod bledu');
      }
    }

}]);


export default app.name;
