import Angular from 'angular';
import NavComponent from '../../components/nav-component.js';
import NavBarRightComponent from '../../components/navbarright-component.js';
import NavController from '../../controllers/nav-controller.js';
import NavbarRightController from '../../controllers/navbarright-controller.js';

let app = Angular.module('nav', [NavComponent, NavController, NavBarRightComponent, NavbarRightController])

export default app.name;
