import Angular from 'angular';
import ExerciseService from '../services/exercise.js';
import CategoryService from '../services/categories.js';
import UserService from '../services/user.js';
import DocumentService from '../services/document.js';


let app = Angular.module('document.modal.update', [ExerciseService, CategoryService, UserService, DocumentService]).component('modalDocumentComponentUpdate', {
  template: require("../templates/document-modal.html"),
  bindings: {
    resolve: '<',
    close: '&',
    dismiss: '&'
  },
  controller: ['$rootScope','user', 'category','exercise', '$scope', 'document', '$location', function ($rootScope, UserService, CategoryService, ExerciseService, $scope, DocumentService, $location) {
    var $ctrl = this;
    $ctrl.$onInit = function () {
      $ctrl.items = $ctrl.resolve.items;
      // $ctrl.selected = {
      //   item: $ctrl.items.name
      // };
      $ctrl.modal_title = "Edycja receptury dokumentu"
      $ctrl.attachments_visible = false;

      $ctrl.document_visible = true;
      $ctrl.document_recipe_visible = true;
      $ctrl.document_recipe_ref_visible = false;
      $ctrl.backendvalid = {}
    };
    $ctrl.ok = function () {
      let objCopy = angular.copy($ctrl.items);
      objCopy.attachments = []; // temporary
      DocumentService.updateDocument("Document.updateDocument.modal", objCopy)
    };

    let registerScope1 = $rootScope.$on('Document.updateDocument.modal', function(event, data) {
      if (data.type === 'success' ) {
        $ctrl.close({$value: 'success'});
      }
      else if (data.type === 'novalid') { $ctrl.backendtype = data.type
        $ctrl.backendvalid = data.msg
        console.log(  $ctrl.backendvalid);
      }
      });
    $scope.$on('$destroy', registerScope1);

    $ctrl.cancel = function () {
      $ctrl.dismiss({$value: 'cancel'});
    };

    var forms = [
      "form1.tpl.html",
      "form2.tpl.html",
      "form3.tpl.html",
    ];
    // $ctrl.single_form = "document.exercise_recipes.tpl.html";
    $ctrl.example="haha";
    $ctrl.displayedForms = [];

    $ctrl.addForm = function(formIndex) {
      $ctrl.displayedForms.push(forms[formIndex]);
    }

    $ctrl.removeForm = function(formIndex) {
      // $ctrl.displayedForms.push(forms[formIndex]);
      // $ctrl.displayedForms.splice(formIndex, 1);
      $ctrl.items.exercise_recipes.splice(formIndex, 1);
    }

    $ctrl.goToSingleExercisePage = function(index){
      window.open("exercise/"+$ctrl.items.exercise_recipes[index].exercise.id, '_blank');
      // $location.path("/exercise/"+$ctrl.items.exercise_recipes[index].exercise.id);
    }
  }]
});

export default app.name;
