import Angular from 'angular';
import AngularBootstrap from 'angular-ui-bootstrap';
import ngAnimate from 'angular-animate';
import ngSanitize from 'angular-sanitize';
import ngTouch from 'angular-touch';

import NavbarView from '../templates/nav-view.html';



let app = Angular.module('nav.component', [AngularBootstrap, ngAnimate, ngSanitize, ngTouch])
  .component('navcomponent', {template: NavbarView})

export default app.name;
