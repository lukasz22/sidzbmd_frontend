import Angular from 'angular';
import ExerciseService from '../services/exercise.js';
import CategoryService from '../services/categories.js';
import UserService from '../services/user.js';

let app = Angular.module('exercise.modal.createa', [ExerciseService, CategoryService, UserService ]).component('modalExerciseComponentCreate', {
  template: require("../templates/exercise-modal.html"),
  bindings: {
    resolve: '<',
    close: '&',
    dismiss: '&'
  },
  controller: ['$rootScope','user', 'category','exercise', '$scope', '$window', function ($rootScope, UserService, CategoryService, ExerciseService, $scope, $window) {
    var $ctrl = this;
    $ctrl.$onInit = function () {

      $ctrl.items = $ctrl.resolve.items;
      console.log($ctrl.items);
      // $ctrl.selected = {
      //   item: $ctrl.items.name
      // };
      $ctrl.attachmentRequired=true;
      $ctrl.items.attachments = [];
      $ctrl.attachments_visible = true;
      $ctrl.deleteSingleAttVisible = true;
      $ctrl.backendvalid = {
        "title" : "",
        "intro": "",
        "task_content" : "",
        "hint": "",
        "solution" : ""
      }


    };
    $ctrl.ok = function () {
      // $ctrl.items.attachments = []; // temporary
      $ctrl.items.owner = UserService.getUsername()
      let objCopy = angular.copy($ctrl.items);
      ExerciseService.createExercise("Exercise.createExercise.modal", objCopy,
                                     $ctrl.selectedLanguage, $ctrl.selectedCategory,
                                     $ctrl.selectedPolicy)
    };

    let registerScope1 = $rootScope.$on('Exercise.createExercise.modal', function(event, data) {
      if (data.type === 'success' ) {
        $ctrl.items.category = $ctrl.selectedCategory;
        $ctrl.items.language = $ctrl.selectedLanguage;
        $ctrl.close({$value: 'success'});
      }
      else if (data.type === 'novalid') { $ctrl.backendtype = data.type
        $ctrl.backendvalid = data.msg
        console.log(  $ctrl.backendvalid);
      }
    });
    $scope.$on('$destroy', registerScope1);

    $ctrl.cancel = function () {
      $ctrl.dismiss({$value: 'cancel'});
    };
    $ctrl.getAccessGroupsProfile = function(){
      UserService.getAccessGroupsProfile('Exercise.create.getAccessGroupsProfile.modal');
    };

    let registerScope2 = $rootScope.$on('Exercise.create.getAccessGroupsProfile.modal', function(event, data) {
      if (data.type === 'success') {
        $ctrl.accessgroup_list=data.msg;
        $ctrl.selectedPolicy=[$ctrl.accessgroup_list[0]];
      }
      });
    $scope.$on('$destroy', registerScope2);

    $ctrl.getLanguages = function(){
      CategoryService.getAllLanguages('Exercise.create.getAllLanguages.modal');
    };

    let registerScope3 = $rootScope.$on('Exercise.create.getAllLanguages.modal', function(event, data) {
      if (data.type === 'success') {
        $ctrl.language_list=data.msg;
        $ctrl.selectedLanguage=$ctrl.language_list[0];
      }
      });
    $scope.$on('$destroy', registerScope3);

    $ctrl.getCategories = function(){
      CategoryService.getAllCategories('Exercise.create.getAllCategoriesTree.modal');
    };

    let registerScope4 = $rootScope.$on('Exercise.create.getAllCategoriesTree.modal', function(event, data) {
      if (data.type === 'success') {
        data.msg=[{id: 'all', name: 'Wszystkie', full_name: "--Żadna--"}, ...data.msg] ;
        $ctrl.category_list=data.msg;
        $ctrl.selectedCategory=$ctrl.category_list[0];
      }
      });
    $scope.$on('$destroy', registerScope4);

    $ctrl.modal_title = "Nowe zadanie";
    $ctrl.getLanguages();
    $ctrl.getCategories();
    $ctrl.getAccessGroupsProfile();

    $ctrl.loadImageFileAsURL = function(index){
      console.log("Jeeee");
      var fileToLoad = $ctrl.items.attachments[index].file;
      var fileReader = new FileReader();
      fileReader.onload = function (fileLoadedEvent) {
        console.log("Jeeee2");
          $ctrl.items.attachments[index].image.base64_string = fileLoadedEvent.target.result;
          $scope.$apply();
          console.log($ctrl.items.attachments[index].image.base64_string);
          console.log(index);
      };
      fileReader.readAsDataURL(fileToLoad);
    }

    // let forms = [{src: "form1.tpl.html",
    //               is_open: true}];

    $ctrl.example="haha";
    $ctrl.displayedForms = [];
    // $ctrl.privateAttachments = [];

    $ctrl.addForm = function(formIndex) {
      $ctrl.displayedForms.push({src: "form1.tpl.html",
                    is_open: true});
      var obj = {};
      obj.image={};
      obj.attachment_type=[
        { "id": "Główna treść zadania",
          "name": "exercise_content"
        },
        { "id": "Wprowadzenie do zadania",
          "name": "exercise_intro"
        },
        { "id": "Wskazówka do zadania",
          "name": "exercise_hint"
        },
        { "id": "Rozwiązanie zadania",
          "name": "exercise_solution"
        }
      ];
      obj.selectedType = obj.attachment_type[0];
      $ctrl.items.attachments.push(obj)
      // $ctrl.items.attachments.splice(formIndex, 0, obj);
      // $ctrl.items.attachments[formIndex].selectedType=$ctrl.items.attachments[formIndex].attachment_type[0];
      console.log($ctrl.items.attachments)
    }

    $ctrl.removeForm = function(formIndex) {
      // $ctrl.displayedForms.push(forms[formIndex]);
      $ctrl.displayedForms.splice(formIndex, 1);
      $ctrl.items.attachments.splice(formIndex, 1);
      console.log($ctrl.items.attachments)
    }

    $ctrl.openInNewWindow = function(index) {
      let image = $ctrl.items.attachments[index].image.base64_string
      $window.open(image)
    }

    $ctrl.goToSingleUserPermissionPage = function(id){
      window.open("user/accessprofiles/"+id, '_blank');
      // $location.path("/exercise/"+$ctrl.items.exercise_recipes[index].exercise.id);
    }
  }]
});

export default app.name;
